import * as AV from 'leancloud-storage'
import  { ApolloServer, gql }  from 'apollo-server';
import { request } from 'http';
async function exec1() {
    const query = new AV.Query('Students');
    const objs = await query.find() as AV.Object[];
    // for(var i =0; i< objs.length;i++){
    //     console.log(objs[i].get('name'));
    // }
    objs.map(it=>{
        console.log(it.get('name'));
    });
}
const books = [
    {
      title: 'Harry Potter and the Chamber of Secrets',
      author: 'J.K. Rowling',
    },
    {
      title: 'Jurassic Park',
      author: 'Michael Crichton',
    },
  ];

  const author = [
      {
          name: 'Mike',
          books: books
      },
      {
          name: 'John',
          books: books
      }
  ]

  const typeDefs = gql`
  type Book {
    title: String
    author: Author
  }
  
  type Author {
    name: String
    books: [Book]
  }

  # The "Query" type is the root of all GraphQL queries.
  # (A "Mutation" type will be covered later on.)
  type Query {
    getBooks(id:Int): Int
    getAuthors(name: String): [Author]
  }

  type Mutation {
    addBook(title: String, author: String): Book
  }
`;

function demo(id:number) {

  return id ? id : 12
}
//https://www.apollographql.com/docs/apollo-server/essentials/schema.html
const resolvers = {
  Query: {
    getBooks: function(args: any){
      return args.id
    },
    getAuthors: () => author,
  },
  Mutation: {
      addBook: (args:{title:string}) => {
          // console.log(Request)
         return books
      },
  }
};

const server = new ApolloServer({ typeDefs, resolvers });

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});