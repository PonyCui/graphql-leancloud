import * as AV from "leancloud-storage";
var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');

// 使用 GraphQL Schema Language 创建一个 schema
var schema = buildSchema(`

  type Query {
    hello(id:Int): String
    login(mobile: Int!, pwd: String!):Boolean
    myToDoList(user_id: String!):[todo]
  }
  type todo {
    name: String
    status: Int!
}
  type Mutation {
    register(mobile: Int!, pwd: String!): Boolean
    addUserTodo(user_id: String!, name: String!): String
    updateUserTodo(user_id: String!, todo_id: String!, new_name: String): Boolean
    deleteUserTodo(user_id: String!, todo_id: String!): Boolean
  }
`);

// root 提供所有 API 入口端点相应的解析器函数
export var root = {
  hello: (args: {id:number}) => {
    return 'Hello world!'+args.id;
  },
  login: async (args: {mobile: number, pwd: string}) => {
    let query = new AV.Query('Demo_User')
    const obj =  await query.equalTo('moblie', args.mobile).equalTo('pwd', args.pwd).first()
    if (obj) {
        return true
    } else {
        return false
    }

  },
  register: async (args: {mobile: number, pwd: string}) => {
      let query = new AV.Query('Demo_User')
      const obj =  await query.equalTo('moblie', args.mobile).first()
      if (typeof obj === 'undefined') {
          let User =  AV.Object.extend('Demo_User')
          let user = new User()
          user.set('moblie', args.mobile)
          user.set('pwd', args.pwd)
          await user.save()
          return true
      } else {
          return false
      }
  }, 

  //用户添加todo
  addUserTodo: async (args: {user_id: string, name: string}) => {
      // 1.先添加到Todo list
      let Todo = AV.Object.extend('Todo')
      let todo = new Todo()
      todo.set('name', args.name)
      const reobj = await todo.save()
      console.log(reobj)
      //2.添加到用户relation[todoList]
      let obj = AV.Object.createWithoutData('Demo_User', args.user_id) as AV.Object
      obj.relation('todoList').add(reobj)
      await obj.save()
      return args.name
  },
  
  //用户修改todo
  updateUserTodo: async (args:{user_id: string,todo_id: string, new_name: string}) => {
        let query = new AV.Query('Demo_User')
        const obj = await query.equalTo('objectId', args.user_id).first() as AV.Object
        if (obj) {
            let relation = obj.relation('todoList')
            const todo = await relation.query().get(args.todo_id)
            if (todo) {
                todo.set('name', args.new_name)
                await todo.save()
                return true
            } else {
                return false
            }
        } else {
            return false
        }
  },

  //用户todo列表
  myToDoList: async (args: {user_id: string}) => {
        let obj = AV.Object.createWithoutData('Demo_User', args.user_id)
        let relation = obj.relation('todoList')
        let todoList = await relation.query().select(['name','status']).find() as AV.Object[]
        todoList.forEach( item => console.log(item.toJSON()))
        return todoList.map(item => item.toJSON())
  },

  //用户删除指定todo
  deleteUserTodo: async (args: {user_id: string, todo_id: string}) => {
    let query = new AV.Query('Demo_User')
    const obj = await query.equalTo('objectId', args.user_id).first() as AV.Object
    if (obj) {
        let relation = obj.relation('todoList')
        const todo = await relation.query().get(args.todo_id) as AV.Object
        if (todo) {
            await relation.remove(todo)
            await todo.destroy()
            return true
        } else {
            return false
        }
    } else {
        return false
    }
  }
};