"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const AV = __importStar(require("leancloud-storage"));
function exec1() {
    return __awaiter(this, void 0, void 0, function* () {
        const query = new AV.Query('Students');
        const objs = yield query.find();
        // for(var i =0; i< objs.length;i++){
        //     console.log(objs[i].get('name'));
        // }
        objs.map(it => {
            console.log(it.get('name'));
        });
    });
}
function exec2() {
    return __awaiter(this, void 0, void 0, function* () {
        const query = new AV.Query('Students');
        const obj = yield query.get('5b9639819f545400398343f1');
        console.log(obj);
        obj.set('age', 23);
        yield obj.save();
    });
}
function exec3() {
    return __awaiter(this, void 0, void 0, function* () {
        const query = new AV.Query('Students');
        const obj = yield query.equalTo('objectId', '5b9639819f545400398343f1');
        console.log(obj);
    });
}
//relations
function exec4() {
    return __awaiter(this, void 0, void 0, function* () {
        const query = new AV.Query('School');
        //query.include('Students');
        const objs = yield query.find();
        for (var i = 0; i < objs.length; i++) {
            // console.log(objs[i].toJSON());
            console.log(objs[i].get('students'));
        }
    });
}
function exec5() {
    return __awaiter(this, void 0, void 0, function* () {
        var Todo = AV.Object.extend('Todo');
        var todo = new Todo();
        todo.set('title', '工程师周会');
        todo.set('content', '每周例会内容');
        todo.set('location', '会议室');
        const res = yield todo.save();
        console.log(res.id);
    });
}
function exec6() {
    return __awaiter(this, void 0, void 0, function* () {
        let TestObject = AV.Object.extend('DataTypeTest');
        let number = 1024;
        let string = 'famous ' + number;
        let date = new Date();
        let array = [string, number];
        let obj = { number: number, string: string };
        let testObject = new TestObject();
        testObject.set('testNumber', number);
        testObject.set('testString', string);
        testObject.set('testDate', date);
        testObject.set('testArray', array);
        testObject.set('testObject', obj);
        const res = yield testObject.save();
        console.log(res);
    });
}
function exec7() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Todo');
        const obj = yield query.get('5b97281b808ca43cd2f66a97');
        console.log(obj);
    });
}
function exec8() {
    return __awaiter(this, void 0, void 0, function* () {
        let obj = AV.Object.createWithoutData('Todo', '5b97281b808ca43cd2f66a97');
        const res = yield obj.fetch();
        console.log(res);
    });
}
function exec9() {
    return __awaiter(this, void 0, void 0, function* () {
        let Todo = AV.Object.extend('Todo');
        let todo = new Todo();
        todo.set('title', 'get id');
        todo.set('content', 'get id');
        const obj = yield todo.save();
        console.log(obj.id);
    });
}
function exec10() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Todo');
        try {
            const obj = yield query.get('5b972dce1579a3003a23b21d');
            //console.log(obj.get('content'));
            console.log(obj.toJSON());
        }
        catch (err) {
            console.log(err);
        }
    });
}
function exec11() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Students');
        query.greaterThanOrEqualTo('age', 30);
        const res = yield query.find();
        res.forEach(item => {
            console.log(item.toJSON());
        });
    });
}
function exec12() {
    return __awaiter(this, void 0, void 0, function* () {
        let todo = AV.Object.createWithoutData('Todo', '5b972782808ca43cd2f663ad');
        todo.set('view', 0);
        try {
            const obj = yield todo.save();
            console.log(obj.get('view'));
            obj.increment('view', 1);
            //obj.fetchWhenSave(true);
            const obj2 = yield obj.save();
            console.log(obj2.get('view'));
        }
        catch (err) {
            console.log(err);
        }
    });
}
//删除对象
function exec13() {
    return __awaiter(this, void 0, void 0, function* () {
        let obj = AV.Object.createWithoutData('Todo', '5b90ebf8ee920a003be0fd13');
        const res = yield obj.destroy();
        console.log(res.toJSON());
    });
}
//批量更新对象
function exec14() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Todo');
        const todos = yield query.find();
        todos.forEach(item => { item.set('status', 1); });
        const res = yield AV.Object.saveAll(todos);
        console.log(res);
    });
}
// AND OR 逻辑查询
function exec15() {
    return __awaiter(this, void 0, void 0, function* () {
        let ageQuery = new AV.Query('Students');
        ageQuery.greaterThan('age', 40);
        let sexQuery = new AV.Query('Students');
        sexQuery.equalTo('sex', '0');
        let query = AV.Query.or(ageQuery, sexQuery);
        const res = yield query.find();
        res.forEach(element => {
            console.log(element.toJSON());
        });
    });
}
//分页 skip limit
function exec16() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Students');
        query.skip(1);
        query.limit(2);
        const res = yield query.find();
        res.forEach(ele => {
            console.log(ele.toJSON());
        });
    });
}
//字符串查询
function exec17() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Students');
        //query.startsWith('name','su');
        //query.contains('name','su');
        //查询name 不包含suzhou的记录
        let regEXP = new RegExp(/^((?!suzhou).)*$/g);
        query.matches('name', regEXP);
        const res = yield query.find();
        res.forEach(ele => {
            console.log(ele.toJSON());
        });
    });
}
//关联查询
function exec18() {
    return __awaiter(this, void 0, void 0, function* () {
        let obj = AV.Object.createWithoutData('School', '5b964b75570c350063fcd9b9');
        //console.log(obj);
        let relations = obj.relation('students');
        let query = relations.query();
        const res = yield query.find();
        res.forEach(ele => {
            console.log(ele.toJSON());
        });
    });
}
//关联查询2
function exec19() {
    return __awaiter(this, void 0, void 0, function* () {
        let obj = AV.Object.createWithoutData('TodoFolder', '5b9767789f545400398f1719');
        //console.log(obj.toJSON());
        let relation = obj.relation('tags');
        let query = relation.query();
        const res = yield query.find();
        res.forEach(ele => {
            console.log(ele.toJSON());
        });
    });
}
//指定查询字段
function exec20() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Students');
        //query.select(['name','sex']);
        const res = yield query.find();
        res.forEach(ele => {
            console.log(ele.toJSON());
        });
    });
}
//非空值查询
function exec21() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Todo');
        //query.exists('title');
        const res = yield query.find();
        res.forEach(ele => {
            console.log(ele.toJSON());
        });
    });
}
//总数
function exec22() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Todo');
        query.equalTo('status', 1);
        const res = yield query.count();
        console.log(res);
    });
}
//poiter
function exec23() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Students');
        var toSchool = AV.Object.createWithoutData('Students', '5b96394f1b69e6005b6d082d');
    });
}
//relation 添加 移除
function exec24() {
    return __awaiter(this, void 0, void 0, function* () {
        let school = yield new AV.Query('School').get('5b964b75570c350063fcd9b9');
        //let student = await new AV.Query('Students').get('5b97841417d0090034ef588c') as AV.Object;
        //console.log(student.toJSON());
        let student = AV.Object.createWithoutData('Students', '5b97841417d0090034ef588c');
        school.relation('students').add(student);
        //school.relation('students').remove(student);
        yield school.save();
        //console.log(res);
        let relation = school.relation('students');
        let data = yield relation.query().find();
        data.forEach(ele => {
            console.log(ele.get('name'));
        });
    });
}
//pointer 一对一
function exec25() {
    return __awaiter(this, void 0, void 0, function* () {
        let student = yield new AV.Query('Students').get('5b978fe9fb4ffe005c888b03');
        let targetSchool = AV.Object.createWithoutData('School', '5b964b75570c350063fcd9b9');
        student.set('targetSchool', targetSchool);
        try {
            yield student.save();
            let query = new AV.Query('Students');
            //关键代码
            query.include('targetSchool');
            const obj = yield query.get('5b978fe9fb4ffe005c888b03');
            console.log(obj.get('targetSchool').get('name'));
        }
        catch (err) {
            console.log(err);
        }
    });
}
//对象中有某个字段为数据类型的查询
function exec26() {
    return __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Students');
        let booksFilter = ['math'];
        query.containsAll('books', booksFilter);
        const obj = yield query.find();
        obj.forEach(ele => {
            console.log(ele.toJSON());
        });
    });
}
exec26();
