"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const AV = __importStar(require("leancloud-storage"));
const apollo_server_1 = require("apollo-server");
function exec1() {
    return __awaiter(this, void 0, void 0, function* () {
        const query = new AV.Query('Students');
        const objs = yield query.find();
        // for(var i =0; i< objs.length;i++){
        //     console.log(objs[i].get('name'));
        // }
        objs.map(it => {
            console.log(it.get('name'));
        });
    });
}
const books = [
    {
        title: 'Harry Potter and the Chamber of Secrets',
        author: 'J.K. Rowling',
    },
    {
        title: 'Jurassic Park',
        author: 'Michael Crichton',
    },
];
const author = [
    {
        name: 'Mike',
        books: books
    },
    {
        name: 'John',
        books: books
    }
];
const typeDefs = apollo_server_1.gql `
  type Book {
    title: String
    author: Author
  }
  
  type Author {
    name: String
    books: [Book]
  }

  # The "Query" type is the root of all GraphQL queries.
  # (A "Mutation" type will be covered later on.)
  type Query {
    getBooks(id:Int): Int
    getAuthors(name: String): [Author]
  }

  type Mutation {
    addBook(title: String, author: String): Book
  }
`;
function demo(id) {
    return id ? id : 12;
}
//https://www.apollographql.com/docs/apollo-server/essentials/schema.html
const resolvers = {
    Query: {
        getBooks: function (args) {
            return args.id;
        },
        getAuthors: () => author,
    },
    Mutation: {
        addBook: (args) => {
            // console.log(Request)
            return books;
        },
    }
};
const server = new apollo_server_1.ApolloServer({ typeDefs, resolvers });
// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});
