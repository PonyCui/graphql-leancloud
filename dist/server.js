"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const AV = __importStar(require("leancloud-storage"));
var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');
// 使用 GraphQL Schema Language 创建一个 schema
var schema = buildSchema(`

  type Query {
    hello(id:Int): String
    login(mobile: Int!, pwd: String!):Boolean
    myToDoList(user_id: String!):[todo]
  }
  type todo {
    name: String
    status: Int!
}
  type Mutation {
    register(mobile: Int!, pwd: String!): Boolean
    addUserTodo(user_id: String!, name: String!): String
    updateUserTodo(user_id: String!, todo_id: String!, new_name: String): Boolean
    deleteUserTodo(user_id: String!, todo_id: String!): Boolean
  }
`);
// root 提供所有 API 入口端点相应的解析器函数
exports.root = {
    hello: (args) => {
        return 'Hello world!' + args.id;
    },
    login: (args) => __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Demo_User');
        const obj = yield query.equalTo('moblie', args.mobile).equalTo('pwd', args.pwd).first();
        if (obj) {
            return true;
        }
        else {
            return false;
        }
    }),
    register: (args) => __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Demo_User');
        const obj = yield query.equalTo('moblie', args.mobile).first();
        if (typeof obj === 'undefined') {
            let User = AV.Object.extend('Demo_User');
            let user = new User();
            user.set('moblie', args.mobile);
            user.set('pwd', args.pwd);
            yield user.save();
            return true;
        }
        else {
            return false;
        }
    }),
    //用户添加todo
    addUserTodo: (args) => __awaiter(this, void 0, void 0, function* () {
        // 1.先添加到Todo list
        let Todo = AV.Object.extend('Todo');
        let todo = new Todo();
        todo.set('name', args.name);
        const reobj = yield todo.save();
        console.log(reobj);
        //2.添加到用户relation[todoList]
        let obj = AV.Object.createWithoutData('Demo_User', args.user_id);
        obj.relation('todoList').add(reobj);
        yield obj.save();
        return args.name;
    }),
    //用户修改todo
    updateUserTodo: (args) => __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Demo_User');
        const obj = yield query.equalTo('objectId', args.user_id).first();
        if (obj) {
            let relation = obj.relation('todoList');
            const todo = yield relation.query().get(args.todo_id);
            if (todo) {
                todo.set('name', args.new_name);
                yield todo.save();
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }),
    //用户todo列表
    myToDoList: (args) => __awaiter(this, void 0, void 0, function* () {
        let obj = AV.Object.createWithoutData('Demo_User', args.user_id);
        let relation = obj.relation('todoList');
        let todoList = yield relation.query().select(['name', 'status']).find();
        todoList.forEach(item => console.log(item.toJSON()));
        return todoList.map(item => item.toJSON());
    }),
    //用户删除指定todo
    deleteUserTodo: (args) => __awaiter(this, void 0, void 0, function* () {
        let query = new AV.Query('Demo_User');
        const obj = yield query.equalTo('objectId', args.user_id).first();
        if (obj) {
            let relation = obj.relation('todoList');
            const todo = yield relation.query().get(args.todo_id);
            if (todo) {
                yield relation.remove(todo);
                yield todo.destroy();
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    })
};
